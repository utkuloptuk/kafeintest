﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace KafeinDbProject
{
    public class County
    {
        public County()
        {
   
        }
        [Key]
        public int CountyId { get; set; }
        public string CountyName { get; set; }

        public ICollection<CityCounty> CityCounties { get; set; }
    }
}
