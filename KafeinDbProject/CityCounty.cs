﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace KafeinDbProject
{
    public class CityCounty
    {
        public CityCounty()
        {

        }
        [Key]
        public int CityCountyId { get; set; }
        public int CityId { get; set; }
        public virtual City City { get; set; }
        public int CountyId { get; set; }
        public virtual County County { get; set; }


    }
}
