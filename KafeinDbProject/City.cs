﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace KafeinDbProject
{
    public class City
    {
        public City()
        {

        }
        [Key]
        public int CityId { get; set; }
        public string CityName { get; set; }

        public ICollection<CityCounty> CityCounties { get; set; }
    }
}
