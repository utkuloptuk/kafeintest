﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KafeinDbProject
{
    public class KafeinDbContext : DbContext
    {
        public static string ConnectionString { get; set; }
        public KafeinDbContext()
        {

        }

        public KafeinDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var options = optionsBuilder.Options.Extensions.FirstOrDefault(e => e.GetType() == typeof(SqlServerOptionsExtension));
            if (options != null)
                ConnectionString = (options as SqlServerOptionsExtension).ConnectionString;
            optionsBuilder.UseSqlServer(ConnectionString);
            base.OnConfiguring(optionsBuilder);
        }

        public DbSet<User> User { get; set; }
        public DbSet<County> County { get; set; }
        public DbSet<City> City { get; set; }
        public DbSet<CityCounty> CityCounty { get; set; }
    }
}
