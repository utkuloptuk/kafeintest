﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace KafeinDbProject
{
    public class User
    {
        public User()
        {

        }
        [Key]
        public int UserId { get; set; }
        public string LoginName { get; set; }
        public string Name { get; set; }
        public string SurName { get; set; }
        public DateTime BirthdayDate { get; set; }
        public int CityId { get; set; }
        public int CountyId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
