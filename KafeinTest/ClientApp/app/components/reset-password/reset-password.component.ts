﻿import { Component, EventEmitter, Output } from '@angular/core';
import { User } from '../../TsObjectFile/UserObjectFile';
import { UserObjectServices } from '../../Services/User';

@Component({
    selector: 'app-reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.css'],
    providers: [UserObjectServices]
})
/** ResetPassword component*/
export class ResetPasswordComponent {
    /** ResetPassword ctor */
    @Output() resetPasswordForm = new EventEmitter<User>();
    processValidation = false;
    statusCode: number = 0;
    requestProcessing = false;
    userData: User = new User;
    constructor(private userServices: UserObjectServices) {

    }


    resetPassword(loginName:string) {

        this.processValidation = true;

        this.preProcessConfigurations();
        let loginNameValue = this.userData
        {
            
                this.userData.LoginName = loginName
        };
        this.resetPasswordForm.emit(loginNameValue);
        debugger;
        this.userServices.resetPass(loginNameValue);

    }



    preProcessConfigurations() {
        this.statusCode = 0;
        this.requestProcessing = true;

    }
}