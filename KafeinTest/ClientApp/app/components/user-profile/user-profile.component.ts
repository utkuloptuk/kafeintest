﻿import { Component, OnInit, EventEmitter, Input } from '@angular/core';
//import { ROUTER_PROVIDERS } from '@angular/router';
import { Router } from '@angular/router';
import { User } from '../../TsObjectFile/UserObjectFile';
import { UserObjectServices } from '../../Services/User';
import { TokenObject } from '../../TsObjectFile/TokenObjectFile';

@Component({
    selector: 'app-user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.css'],
    providers: [UserObjectServices]
})
/** UserProfile component*/
export class UserProfileComponent  {
    /** UserProfile ctor */
    userData: User = new User;
    tokenData: TokenObject = new TokenObject;
    token: string='';
    statusCode: number = 0;
    @Input() userForm = new EventEmitter<TokenObject>();
    constructor(private router: Router, private userServices: UserObjectServices) {
        debugger;
        this.token = JSON.stringify(localStorage.getItem("Token"));
        this.getUserFunc(this.token);
    }

    getUserFunc(userToken: string) {
        
        let tokenValues = this.tokenData
        {
            this.tokenData.Token = userToken
        }

        this.userForm.emit(tokenValues);
        this.userServices.getUser(tokenValues)
            .toPromise()
            .then((data: any) => {
                debugger
                this.userData = data;
            }
            , (errorCode: any) => this.statusCode = errorCode
            );
    }

    //getUserIdFunc() {
    //    this.userForm.emit();
    //    this.userServices.getUserId()
    //        .toPromise()
    //        .then((data: any) => {
    //            this.userData = data;
    //        }
    //            , (errorCode: any) => this.statusCode = errorCode
    //        );
    //}

    redirect() {
        this.router.navigate(['./userEdit']);
    }
}