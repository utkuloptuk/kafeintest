﻿import { Component, OnInit, Input, EventEmitter, AfterViewInit, Output } from '@angular/core';
import { CountyObjectServices } from '../../Services/City';
import { County } from '../../TsObjectFile/CountyObjectFile';
import { User } from '../../TsObjectFile/UserObjectFile';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { UserObjectServices } from '../../Services/User';
import { provideForRootGuard } from '@angular/router/src/router_module';
import { toPromise } from 'rxjs/operator/toPromise';
import { City } from '../../TsObjectFile/CityObjectFile';
import * as moment from 'moment';

@Component({
    selector: 'app-user-register',
    templateUrl: './user-register.component.html',
    styleUrls: ['./user-register.component.css'],
    providers: [CountyObjectServices, UserObjectServices]

})
/** UserRegister component*/
export class UserRegisterComponent implements OnInit {
    /** UserRegister ctor */

    countyArea: County[] | undefined;
    userData: User = new User;
    birthdayDate: moment.Moment = moment();
    countyData: County = new County;
    cityData: City | undefined;
    //city: string = '';

    @Input() PostSubjectCounty = new EventEmitter<County>();
    @Output() userCreateForm = new EventEmitter<User>();
    processValidation = false;
    statusCode: number = 0;
    requestProcessing = false;

    //userForm = new FormGroup({
    //    Name: new FormControl('', Validators.required),
    //    SurName: new FormControl('', Validators.required),
    //    Email: new FormControl('', Validators.required),
    //    BirthdayDate: new FormControl('', Validators.required),
    //    Password: new FormControl('', Validators.required),
    //    LoginName: new FormControl('', Validators.required)
    //});

    settings = {
        bigBanner: true,
        timePicker: false,
        format: 'dd-MM-yyyy',
        defaultOpen: true
    }
    constructor(private countyServices: CountyObjectServices, private userServices: UserObjectServices) {

    }
    ngOnInit() {
        //debugger;
        //this.getCountyVariables("Ankara");
    }

    getCountyVariables(getValue: string) {
        debugger;
        let cityValue = this.countyData
        {
            this.countyData.cityName = getValue

        }
        this.PostSubjectCounty.emit(cityValue);
        this.countyServices.getCounty(cityValue)
            .toPromise()
            .then(
                (data: any) => {
                    debugger
                    this.countyArea = data;
                }
            );
    }

    getUserCreateData(cityName: string, countyId: number, email: string, loginName: string, name: string, password: string, surName: string, birthdayDate: string) {

        this.processValidation = true;
        //birthdayDate = this.birthdayDate;

        this.preProcessConfigurations();
        let userValue = this.userData
        {
            this.userData.cityName = cityName,
                this.userData.CountyId = countyId,
                this.userData.Email = email,
                this.userData.BDate = birthdayDate,
                this.userData.LoginName = loginName,
                this.userData.Name = name,
                this.userData.Password = password,
                this.userData.SurName = surName
        };
        this.userCreateForm.emit(userValue);
        debugger;
        this.userServices.createUser(userValue);

    }

    //getCountyId(countyName: string) {
    //    let countyValue = new
    //}
    onDateSelect(date: moment.Moment) {
        let Bdate = date;
        this.birthdayDate = Bdate;
    }
    preProcessConfigurations() {
        this.statusCode = 0;
        this.requestProcessing = true;

    }


}