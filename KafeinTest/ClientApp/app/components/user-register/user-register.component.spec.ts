﻿/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { UserRegisterComponent } from './user-register.component';

let component: UserRegisterComponent;
let fixture: ComponentFixture<UserRegisterComponent>;

describe('UserRegister component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ UserRegisterComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(UserRegisterComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});