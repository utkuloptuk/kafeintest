﻿import { Component, Output, EventEmitter, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../../TsObjectFile/UserObjectFile';
import { UserObjectServices } from '../../Services/User';
import { TokenObject } from '../../TsObjectFile/TokenObjectFile';
import { TokenObjectServices } from '../../Services/Token';

@Component({
    selector: 'app-user-login',
    templateUrl: './user-login.component.html',
    styleUrls: ['./user-login.component.css'],
    providers: [ UserObjectServices,TokenObjectServices]
})
/** UserLogin component*/
export class UserLoginComponent implements OnInit {
    /** UserLogin ctor */
    userData: User = new User;
    @Output() userLoginForm = new EventEmitter<User>();
    @Input() tokenForm = new EventEmitter<User>();
    tokenValues: string = '';
    beniHatirla = false;
    processValidation = false;
    statusCode: number = 0;
    requestProcessing = false;
    constructor(private router: Router, private userServices: UserObjectServices, private tokenServices: TokenObjectServices) {
    
    }

    ngOnInit() {
        this.checkboxControl(this.userData.LoginName, this.userData.Password);
    }

    redirect() {
        this.router.navigate(['./resetPassword']);
    }

    loginEvent(loginName: string, pass: string) {
        this.processValidation = true;
        debugger
        this.preProcessConfigurations();
        let userValue = this.userData
        {
            this.userData.LoginName = loginName,
                this.userData.Password = pass
        };

        this.userLoginForm.emit(userValue);
        this.userLoginFunc(loginName, pass);
        //this.tokenForm.emit(userValue);
        this.getTokenFunc(loginName, pass);

    }

    userLoginFunc(loginName: string, pass: string) {
        this.processValidation = true;
        debugger
        this.preProcessConfigurations();
        let userValue = this.userData
        {
            this.userData.LoginName = loginName,
                this.userData.Password=pass
        };

        this.userLoginForm.emit(userValue);
        this.userServices.loginUser(userValue);
    }

    getTokenFunc(loginName: string, pass: string) {
        this.processValidation = true;
        debugger
        this.preProcessConfigurations();
        let userValue = this.userData
        {
            this.userData.LoginName = loginName,
                this.userData.Password = pass
        };

        this.tokenForm.emit(userValue);
        this.tokenServices.getToken(userValue)
            .toPromise()
            .then(
            (data: any) => {
                debugger
                this.tokenValues = data.token;
                    if (this.beniHatirla == true) {
                        localStorage.setItem("Token", this.tokenValues);
                        this.redirectProfile();
                    }
                    else
                        localStorage.setItem("Token", "");
                
            },
            (errorCode:any) => this.statusCode = errorCode
            );
        //this.tokenValues = JSON.stringify( this.tokenServices.getToken(userValue));
        debugger;
        
    }

    checkboxControl(loginName: string, pass: string) {
        if (this.beniHatirla == true) {
            this.getTokenFunc(loginName, pass);
        }
        else {
            localStorage.setItem("Token", "");
        }
    }

    redirectProfile() {
        this.router.navigate(['./userProfile']);
    }
    preProcessConfigurations() {
        this.statusCode = 0;
        this.requestProcessing = true;

    }

    

}