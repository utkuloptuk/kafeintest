﻿import { Component, EventEmitter, Output, Input } from '@angular/core';
import { User } from '../../TsObjectFile/UserObjectFile';
import { UserObjectServices } from '../../Services/User';
import { TokenObject } from '../../TsObjectFile/TokenObjectFile';

@Component({
    selector: 'app-user-edit',
    templateUrl: './user-edit.component.html',
    styleUrls: ['./user-edit.component.css'],
    providers: [UserObjectServices]
})
/** UserEdit component*/
export class UserEditComponent {
    /** UserEdit ctor */
    @Input() userForm = new EventEmitter<TokenObject>();
    @Output() userEditedForm = new EventEmitter<User>();
    processValidation = false;
    statusCode: number = 0;
    requestProcessing = false;
    userData: User = new User;
    tokenData: TokenObject = new TokenObject;
    token: string = '';

    constructor(private userServices: UserObjectServices) {
        this.token = JSON.stringify(localStorage.getItem("Token"));
        this.getUserFunc(this.token);
    }

    getUserFunc(userToken: string) {

        let tokenValues = this.tokenData
        {
            this.tokenData.Token = userToken
        }

        this.userForm.emit(tokenValues);
        this.userServices.getUser(tokenValues)
            .toPromise()
            .then((data: any) => {
                debugger
                this.userData = data;
            }
                , (errorCode: any) => this.statusCode = errorCode
            );
    }
    usereditedData(Name: string, SurName: string, LoginName: string, Email: string, Password: string) {

        this.processValidation = true;

        this.preProcessConfigurations();
        let userValue = this.userData
        {
            debugger
            this.userData.UserId = this.userData.UserId,
                this.userData.Email = Email,
                this.userData.LoginName = LoginName,
                this.userData.Name = Name,
                this.userData.Password = Password,
                this.userData.SurName = SurName
        };
        this.userEditedForm.emit(userValue);
        debugger;
        this.userServices.userEdited(userValue);

    }

    preProcessConfigurations() {
        this.statusCode = 0;
        this.requestProcessing = true;

    }

}