﻿export class County {

    public cityName: string;
    public countyId: number;
    public countyName: string;
    constructor() {
        this.countyId = 0;
        this.countyName = '';
        this.cityName = '';
    }

}