﻿
import * as moment from 'moment';
export class User {
    public UserId: number;
    public Name: string;
    public SurName: string;
    public BDate: string;
    public CityId: number;
    public CountyId: number;
    public Email: string;
    public LoginName: string;
    public Password: string;
    public cityName: string;

    constructor() {
        this.UserId = 0;
        this.Name = '';
        this.SurName = '';
        this.BDate = '';
        this.CityId = 0;
        this.CountyId= 0;
        this.Email= '';
        this.LoginName= '';
        this.Password = '';
        this.cityName = '';

    }

}