﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';

import { County } from '../TsObjectFile/CountyObjectFile'


@Injectable()

export class CountyObjectServices {

    countyGetUrl = "http://localhost:50220/api/SelectedCityGetCounty";


    constructor(private http: Http) {

    }

    getCounty(county: County): Observable<County[]> {
        let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });
        debugger;
        return this.http.post(this.countyGetUrl, county, options)
            .map((res:any) => {
                return res.json()
            });
    }

}