﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';

import { County } from '../TsObjectFile/CountyObjectFile'
import { User } from '../TsObjectFile/UserObjectFile';
import { Token } from '@angular/compiler';
import { TokenObject } from '../TsObjectFile/TokenObjectFile';


@Injectable()

export class UserObjectServices {

    createUserUrl = "http://localhost:50220/api/CreateUser";
    userResPassUrl = "http://localhost:50220/api/resetPassword";
    userLoginUrl = "http://localhost:50220/api/loginProcess";
    userEditedUrl = "http://localhost:50220/api/userEdited";
    userGetUrl = "http://localhost:50220/api/getUser";

    constructor(private http: Http) {

    }

    createUser(userData: User): any {
        debugger;
        let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });

        return this.http.post(this.createUserUrl, userData, options)
            .toPromise()
            .then(res => {
                return res.status;
            }).catch(this.handleError);
    }

    resetPass(userData: User): any {
        let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });
        return this.http.post(this.userResPassUrl, userData, options)
            .toPromise()
            .then(res => {
                return res.status;
            }).catch(this.handleError);
    }

    loginUser(userData: User):any {
        let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });
        return this.http.post(this.userLoginUrl, userData, options)
            .toPromise()
            .then(res => {
                return res.status;
            }).catch(this.handleError);

    }

    userEdited(userData: User): any {
        debugger;
        let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });

        return this.http.post(this.userEditedUrl, userData, options)
            .toPromise()
            .then(res => {
                return res.status;
            }).catch(this.handleError);
    }

    getUser(token: TokenObject): Observable<User> {
        let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });
        return this.http.post(this.userGetUrl,token, options)
            .map((res: any) => {
                return res.json()
            });
    }



    

    private handleError(error: Response | any) {
        console.error(error.message || error);

        return Observable.throw(error.status);
    }


}