﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { UserLogin } from '../TsObjectFile/LoginObjectFile';
import { TokenObject } from '../TsObjectFile/TokenObjectFile';
import { User } from '../TsObjectFile/UserObjectFile';
import { Token } from '@angular/compiler';




@Injectable()

export class TokenObjectServices {

    getTokenUrl = "http://localhost:50220/token";


    constructor(private http: Http) {

    }

    getToken(userData: UserLogin): Observable<TokenObject>  {
        let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });
        debugger;
        return this.http.post(this.getTokenUrl, JSON.stringify(userData), options)
            .map(this.extractData)
            .catch(this.handleError);
            

    }



    private extractData(res: Response) {
        let body = res.json();
        return body;
    }

    private handleError(error: Response | any) {
        console.error(error.message || error);

        return Observable.throw(error.status);
    }
}