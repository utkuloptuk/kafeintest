﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using KafeinDbProject;
using KafeinTest.Interfaces;
using KafeinTest.Model;
using KafeinTest.Model.DataTransferObject;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace KafeinTest.Controllers
{
    public class UserController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        private IUserManager _IUserManager;
        public UserController(IUserManager userManager)
        {
            _IUserManager = userManager;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/getUser")]
        public ActionResult GetUser([FromBody]DTOTokenModel token)
        {
            int uId = getUserId(token.Token);
            return Ok(_IUserManager.Get(uId));
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/resetPassword")]
        public ActionResult ResetPassword([FromBody]User user)
        {
            using (KafeinDbContext context = new KafeinDbContext())
            {
                var oldUser = context.User.FirstOrDefault(x => x.LoginName == user.LoginName);
                if (oldUser != null)
                {
                    var resPass = new User()
                    {
                        BirthdayDate = oldUser.BirthdayDate,
                        Password = "111111",
                        CityId = oldUser.CityId,
                        CountyId = oldUser.CountyId,
                        Email = oldUser.Email,
                        LoginName = oldUser.LoginName,
                        Name = oldUser.Name,
                        SurName = oldUser.SurName,
                        UserId = oldUser.UserId

                    };
                    _IUserManager.Update(resPass);
                    return Json(new Result(true, "işlem basarılı"));
                }
                else
                {
                    return Json(new Result(false, "kullanıcı bulunamadı"));
                }
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/getUserInformation")]
        public ActionResult getUserInformation([FromBody]DTOTokenModel token)
        {
            int UserId = getUserId(token.Token);

            return Json(_IUserManager.Get(UserId));
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/userEdited")]
        public ActionResult UserEdited([FromBody]User user)
        {
            
            using (KafeinDbContext context = new KafeinDbContext())
            {
                var oldUserValues = _IUserManager.Get(user.UserId);
                try
                {
                    var us = new User()
                    {
                        UserId=oldUserValues.UserId,
                        Name = String.IsNullOrEmpty(user.Name) ? oldUserValues.Name:user.Name,
                        SurName = String.IsNullOrEmpty(user.SurName) ? oldUserValues.SurName:user.SurName,
                        BirthdayDate = user.BirthdayDate == DateTime.MinValue ? oldUserValues.BirthdayDate : user.BirthdayDate,
                        CityId = user.CityId != 0 ? oldUserValues.CityId : user.CityId,
                        CountyId = user.CountyId != 0 ? oldUserValues.CountyId : user.CountyId,
                        Email = String.IsNullOrEmpty(user.Email) ? oldUserValues.Email:user.Email,
                        LoginName = string.IsNullOrEmpty(user.LoginName) ? oldUserValues.LoginName:user.LoginName,
                        Password = string.IsNullOrEmpty(user.Password) ? oldUserValues.Password:user.Password
                    };
                    _IUserManager.Update(us);
                    return Json(new Result(true, "Işlem başarılı"));
                }
                catch
                {
                    return Json(new Result(false, "db işlemi başarısız"));
                }
            }
        }


        public int getUserId(string Token)
        {
            int userId = -1;


            //string authToken = HttpContext.Request.Headers["Authorization"].ToString();

            var authBaseToken2 = Token.Split(" ")[1];
            var authBaseToken= authBaseToken2.Split(" ")[0];
            if (!string.IsNullOrEmpty(authBaseToken))
            {
                var tokens = jwtDecode(authBaseToken);
                userId = Convert.ToInt32(tokens.Payload.First(c => c.Key == "sid").Value);
                return userId;
            }
            else
            {
                return userId;
            }
        }



        private JwtSecurityToken jwtDecode(string authToken)
        {

            var ctoken = new JwtSecurityToken(authToken);
            return ctoken;
        }
    }
}