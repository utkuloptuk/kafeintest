﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using KafeinDbProject;
using KafeinTest.Model.DataTransferObject;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace KafeinTest.Controllers
{
    public class TokenController : Controller
    {
        private IConfiguration _configuration;

        public TokenController(IConfiguration Conf)
        {
            _configuration = Conf;
        }


        [HttpPost]
        [AllowAnonymous]
        [Route("token")]
        public IActionResult Post([FromBody]DTOUserModel dto)
        {

            if (ModelState.IsValid)
            {
                var userId = GetUserIdFromCredentials(dto);
                if (userId.Equals("-1"))
                {
                    return Unauthorized();
                }
                var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sid,userId),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                };
                var token = new JwtSecurityToken
                    (
                    issuer: _configuration["Issuer"],
                    audience: _configuration["Issuer"],
                    claims: claims,
                    expires: DateTime.UtcNow.AddDays(60),
                    notBefore: DateTime.UtcNow,
                    signingCredentials: new SigningCredentials
                    (new SymmetricSecurityKey(
                        Encoding.UTF8.GetBytes(_configuration["Jwt:Key"])),
                    SecurityAlgorithms.HmacSha256)
                    );
                return Json(new { token = "Bearer " + new JwtSecurityTokenHandler().WriteToken(token)+" " });
            }
            return BadRequest();
        }
        public string GetUserIdFromCredentials(DTOUserModel dto)
        {
            using (KafeinDbContext context=new KafeinDbContext())
            {
                int userid = -1;
                if (context.User.Any(x => x.LoginName == dto.LoginName  && x.Password == dto.Password) == true)
                {
                    userid = context.User.Where(l => l.LoginName == dto.LoginName && l.Password == dto.Password).FirstOrDefault().UserId;
                    return userid.ToString();

                }
                else
                {
                    return userid.ToString();
                }
            }
        }
    }
}