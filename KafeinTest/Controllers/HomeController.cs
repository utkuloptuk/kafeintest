using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using KafeinDbProject;
using Microsoft.AspNetCore.Mvc;

namespace KafeinTest.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index(KafeinDbContext db)
        {
            //Onceden eklenmesi gereken db dee�erleri
            if (db.City.Count().Equals(0))
            {
                db.City.Add(new City { CityName = "Edirne" });
                db.City.Add(new City { CityName = "Ankara" });
                db.City.Add(new City { CityName = "Istanbul" });
                db.City.Add(new City { CityName = "Manisa" });
                db.SaveChanges();
            }
            if(db.County.Count().Equals(0))
            {
                db.County.Add(new County { CountyName="Kesan"});
                db.County.Add(new County { CountyName="Ipsala"});
                db.County.Add(new County { CountyName="Meric"});
                db.County.Add(new County { CountyName="Cankaya"});
                db.County.Add(new County { CountyName="Etimesgut"});
                db.County.Add(new County { CountyName="K�z�lay"});
                db.County.Add(new County { CountyName="Kecioren"});
                db.County.Add(new County { CountyName="Kadik�y"});
                db.County.Add(new County { CountyName="Atasehir"});
                db.County.Add(new County { CountyName="Maslak"});
                db.County.Add(new County { CountyName="Besiktas"});
                db.County.Add(new County { CountyName = "Bostanci"});
                db.County.Add(new County { CountyName="Maltepe"});
                db.County.Add(new County { CountyName="Salihli"});
                db.County.Add(new County { CountyName="Akhisar"});
                db.County.Add(new County { CountyName="Turgutlu"});
                db.SaveChanges();

            }
            if(db.CityCounty.Count().Equals(0))
            {
                db.CityCounty.Add(new CityCounty { CityId=1, CountyId=1});
                db.CityCounty.Add(new CityCounty { CityId = 1, CountyId = 2 });
                db.CityCounty.Add(new CityCounty { CityId = 1, CountyId = 3 });
                db.CityCounty.Add(new CityCounty { CityId = 2, CountyId = 4 });
                db.CityCounty.Add(new CityCounty { CityId = 2, CountyId = 5 });
                db.CityCounty.Add(new CityCounty { CityId = 2, CountyId = 6 });
                db.CityCounty.Add(new CityCounty { CityId = 2, CountyId = 7 });
                db.CityCounty.Add(new CityCounty { CityId = 3, CountyId = 8 });
                db.CityCounty.Add(new CityCounty { CityId = 3, CountyId = 9 });
                db.CityCounty.Add(new CityCounty { CityId = 3, CountyId = 10 });
                db.CityCounty.Add(new CityCounty { CityId = 3, CountyId = 11 });
                db.CityCounty.Add(new CityCounty { CityId = 3, CountyId = 12 });
                db.CityCounty.Add(new CityCounty { CityId = 3, CountyId = 13 });
                db.CityCounty.Add(new CityCounty { CityId = 4, CountyId = 14 });
                db.CityCounty.Add(new CityCounty { CityId = 4, CountyId = 15 });
                db.CityCounty.Add(new CityCounty { CityId = 4, CountyId = 16 });
                db.SaveChanges();
            }
            if(db.User.Count().Equals(0))
            {
                db.User.Add(new User {BirthdayDate=DateTime.Now,CityId=1,CountyId=1,Email="admin@gmail.com",LoginName="admin", Name="admin",Password="111111",SurName="admin" });
                db.User.Add(new User { BirthdayDate = DateTime.Now, CityId = 1, CountyId = 1, Email = "user@gmail.com", LoginName = "user", Name = "user", Password = "111111", SurName = "user" });
                db.SaveChanges();
            }

            return View();
        }

        public IActionResult Error()
        {
            ViewData["RequestId"] = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
            return View();
        }
    }
}
