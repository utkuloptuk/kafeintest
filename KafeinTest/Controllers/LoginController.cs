﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KafeinDbProject;
using KafeinTest.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace KafeinTest.Controllers
{
    public class LoginController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public LoginController()
        {

        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/loginProcess")]
        public ActionResult LoginProcess([FromBody]User user)
        {
            using (KafeinDbContext context = new KafeinDbContext())
            {
                try
                {
                    if (context.User.Any(x => x.LoginName == user.LoginName && x.Password == user.Password))
                    {
                        return Json(new Result(true, "Giriş işlemi başarılı"));
                    }
                    else
                    {
                        return Json(new Result(false, "Giriş işlemi başarısız"));
                    }
                }
                catch(Exception e)
                {
                    return Json(new Result(false,e.ToString()));
                }
            }
        }
    }
}