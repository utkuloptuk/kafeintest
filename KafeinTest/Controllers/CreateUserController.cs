﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation.Results;
using KafeinDbProject;
using KafeinTest.BLL.Concrete;
using KafeinTest.Interfaces;
using KafeinTest.Model;
using KafeinTest.Model.DataTransferObject;
using KafeinTest.Model.Validation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace KafeinTest.Controllers
{
    public class CreateUserController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        private IUserManager _IUsermanager;

        public CreateUserController(IUserManager userManager)
        {
            _IUsermanager = userManager;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/CreateUser")]
        public ActionResult CreateUser([FromBody]DTOUser user)
        {

            UserValidation uv = new UserValidation();
            ValidationResult result = uv.Validate(user);
            string dtString = user.BDate;
            dtString = dtString.Replace("(GMT+03:00)", "").Trim();
            string format = "ddd MMM dd yyyy HH:mm:ss 'GMT'K";
            DateTime date;
            bool validFormat = DateTime.TryParseExact(dtString, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out date);
            using (KafeinDbContext context = new KafeinDbContext())
            {
                if (context.User.Any(x => x.LoginName == user.LoginName))
                {
                    return Json(new Result(false, "Bu isimli bir kullanıcı ismi var"));
                }
                else if (context.User.Any(x => x.Email == user.Email))
                {
                    return Json(new Result(false, "Bu email adresine kayıtlı biri var"));
                }
                else
                {
                    try
                    {
                        if (result.IsValid)
                        {
                            var us = new User()
                            {
                                Name = user.Name,
                                SurName = user.SurName,
                                BirthdayDate = date,
                                CityId = context.City.FirstOrDefault(x=>x.CityName==user.CityName).CityId,
                                CountyId = user.CountyId,
                                Email = user.Email,
                                LoginName = user.LoginName,
                                Password = user.Password
                            };
                            _IUsermanager.Add(us);
                            return Json(new Result(true, "Işlem başarılı"));
                        }

                        else
                        {
                            foreach(var failure in result.Errors)
                            {
                                Console.WriteLine("Property" + failure.PropertyName + "failure validation. Error was: " + failure.ErrorMessage);
                            }
                            return Json(
                                new Result(
                                    false,
                                    result.Errors.ToString()
                                    ));
                        }
                    }
                    catch(DbException dExp)
                    {
                        return Json(new Result(false,"db işlemi başarısız"));
                    }
                }
            }
        }
    }
}