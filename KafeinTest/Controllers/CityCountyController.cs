﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KafeinDbProject;
using KafeinTest.Interfaces;
using KafeinTest.Model;
using KafeinTest.Model.DataTransferObject;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace KafeinTest.Controllers
{
    public class CityCountyController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        private ICityManager _ICityManager;
        private ICountyManager _ICountyManager;
        public CityCountyController(ICityManager cityManager, ICountyManager countyManager)
        {
            _ICityManager = cityManager;
            _ICountyManager = countyManager;
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/GetCity")]
        public ActionResult GetCityAll()
        {
            var cityList = _ICityManager.GetAll();
            return Ok(cityList);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/SelectedCityGetCounty")]
        public ActionResult CitySelectedGetCounty([FromBody]DTOCityModel cityModel)
        {
            using (KafeinDbContext context = new KafeinDbContext())
            {
                if (context.City.Any(x=>x.CityName==cityModel.cityName))
                {

                    try
                    {
                        var selectedCityGetCounty = (from city in context.City.Where(x=>x.CityName==cityModel.cityName)
                                                     join cityCounty in context.CityCounty on city.CityId equals cityCounty.CityId
                                                     join county in context.County on cityCounty.CountyId equals county.CountyId
                                                     select new
                                                     {
                                                         city.CityName,
                                                         county.CountyId,
                                                         county.CountyName,
                                                     }).ToList();
                        if (selectedCityGetCounty.Any())
                            return Ok(selectedCityGetCounty);
                        else
                        {
                            return Json(new Result(false, "İlgili sehirde bir kayıt bulunmamakta"));
                        }
                    }
                    catch (Exception e)
                    {
                        return Json(new Result(false, "Db hatası"));
                    }
                }
                else
                {
                    return Json(new Result(false, "sehir bilgini bulamadım"));
                }
            }
      
        }
    }
}