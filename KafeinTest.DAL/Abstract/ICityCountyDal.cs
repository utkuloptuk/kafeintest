﻿using KafeinDbProject;
using System;
using System.Collections.Generic;
using System.Text;

namespace KafeinTest.DAL.Abstract
{
    public interface ICityCountyDal
    {
        List<CityCounty> GetAll();
        CityCounty Get(int cityCountyId);
        void Add(CityCounty cityCounty);
        void Delete(int cityCountyId);
        void Update(CityCounty cityCounty);
    }
}
