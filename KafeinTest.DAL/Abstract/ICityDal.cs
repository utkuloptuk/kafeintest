﻿using KafeinDbProject;
using System;
using System.Collections.Generic;
using System.Text;

namespace KafeinTest.DAL.Abstract
{
    public interface ICityDal
    {
        List<City> GetAll();
        City Get(int cityId);
        void Add(City city);
        void Delete(int cityId);
        void Update(City city);
    }
}
