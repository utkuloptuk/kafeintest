﻿using KafeinDbProject;
using KafeinTest.DAL.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KafeinTest.DAL.Concrete
{
    public class EFCityDal : ICityDal
    {
        public EFCityDal()
        {
        }

        public void Add(City city)
        {
            using (KafeinDbContext context = new KafeinDbContext())
            {
                context.City.Add(city);
                context.SaveChanges();
            }
        }

        public void Delete(int cityId)
        {
            using (KafeinDbContext context = new KafeinDbContext())
            {
                context.City.Remove(context.City.FirstOrDefault(x=>x.CityId==cityId));
                context.SaveChanges();
            }
        }

        public City Get(int cityId)
        {
            using (KafeinDbContext context = new KafeinDbContext())
            {
                City cities = context.City.FirstOrDefault(x=>x.CityId==cityId);
                return cities;
            }
        }

        public List<City> GetAll()
        {
            using (KafeinDbContext context = new KafeinDbContext())
            {
                return context.City.ToList();
            }
        }

        public void Update(City city)
        {
            using (KafeinDbContext context = new KafeinDbContext())
            {
                City oldCities = context.City.FirstOrDefault(x=>x.CityId==city.CityId);

                oldCities.CityName = city.CityName;
                context.SaveChanges();
            }
        }
    }
}
