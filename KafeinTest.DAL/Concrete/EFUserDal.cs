﻿using KafeinDbProject;
using KafeinTest.DAL.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KafeinTest.DAL.Concrete
{
    public class EFUserDal : IUserDal
    {
        public EFUserDal()
        {
        }

        public void Add(User user)
        {
            using (KafeinDbContext context = new KafeinDbContext())
            {
                context.User.Add(user);
                context.SaveChanges();
            }
        }

        public void Delete(int userId)
        {
            using (KafeinDbContext context = new KafeinDbContext())
            {
                context.User.Remove(context.User.FirstOrDefault(x=>x.UserId==userId));
                context.SaveChanges();
            }
        }

        public User Get(int userId)
        {
            using (KafeinDbContext context = new KafeinDbContext())
            {
                return context.User.FirstOrDefault(x=>x.UserId==userId);
            }
        }

        public List<User> GetAll()
        {
            using (KafeinDbContext context = new KafeinDbContext())
            {
                return context.User.ToList();
            }
        }

        public void Update(User user)
        {
            using (KafeinDbContext context=new KafeinDbContext())
            {
                User oldUser = context.User.FirstOrDefault(x=>x.UserId==user.UserId);

                oldUser.BirthdayDate = user.BirthdayDate;
                oldUser.CityId = user.CityId;
                oldUser.CountyId = user.CountyId;
                oldUser.Email = user.Email;
                oldUser.Name = user.Name;
                oldUser.Password = user.Password;
                oldUser.SurName = user.SurName;
                oldUser.LoginName = user.LoginName;
                context.SaveChanges();

            }
        }
    }
}
