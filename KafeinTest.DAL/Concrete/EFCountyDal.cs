﻿using KafeinDbProject;
using KafeinTest.DAL.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KafeinTest.DAL.Concrete
{
    public class EFCountyDal : ICountyDal
    {
        public EFCountyDal()
        {
        }

        public void Add(County county)
        {
            using (KafeinDbContext context = new KafeinDbContext())
            {
                context.County.Add(county);
                context.SaveChanges();
            }
        }

        public void Delete(int countyId)
        {
            using (KafeinDbContext context = new KafeinDbContext())
            {
                context.County.Remove(context.County.FirstOrDefault(x=>x.CountyId==countyId));
                context.SaveChanges();
            }
        }

        public County Get(int countyId)
        {
            using (KafeinDbContext context = new KafeinDbContext())
            {
                return context.County.FirstOrDefault(x=>x.CountyId==countyId);
            }
        }

        public List<County> GetAll()
        {
            using (KafeinDbContext context = new KafeinDbContext())
            {
                return context.County.ToList();
            }
        }

        public void Update(County county)
        {
            using (KafeinDbContext context = new KafeinDbContext())
            {
                County oldCounty = context.County.FirstOrDefault(x=>x.CountyId==county.CountyId);

                oldCounty.CountyName = county.CountyName;
                context.SaveChanges();
            }
        }
    }
}
