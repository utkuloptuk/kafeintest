﻿using KafeinDbProject;
using KafeinTest.DAL.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KafeinTest.DAL.Concrete
{
    public class EFCityCountyDal : ICityCountyDal
    {
        public EFCityCountyDal()
        {
        }
        //KafeinDbContext context = new KafeinDbContext();
        public void Add(CityCounty cityCounty)
        {
            using (KafeinDbContext context = new KafeinDbContext())
            {
                context.CityCounty.Add(cityCounty);
                context.SaveChanges();
            }
        }

        public void Delete(int cityCountyId)
        {
            using (KafeinDbContext context = new KafeinDbContext())
            {
                context.CityCounty.Remove(context.CityCounty.FirstOrDefault(x=>x.CityCountyId==cityCountyId));
                context.SaveChanges();
            }
        }

        public CityCounty Get(int cityCountyId)
        {
            using (KafeinDbContext context = new KafeinDbContext())
            {
                CityCounty cityCounty = context.CityCounty.FirstOrDefault(x=>x.CityCountyId==cityCountyId);
                return cityCounty;
            }
        }

        public List<CityCounty> GetAll()
        {
            using (KafeinDbContext context = new KafeinDbContext())
            {
                return context.CityCounty.ToList();
            }
        }

        public void Update(CityCounty cityCounty)
        {
            using (KafeinDbContext context = new KafeinDbContext())
            {
                CityCounty oldCityCounty = context.CityCounty.FirstOrDefault(x=>x.CityCountyId==cityCounty.CityCountyId);

                oldCityCounty.CityId = cityCounty.CityId;
                oldCityCounty.CountyId = cityCounty.CountyId;
                context.SaveChanges();
            }
        }
    }
}
