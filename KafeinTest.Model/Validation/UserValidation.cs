﻿using FluentValidation;
using KafeinDbProject;
using System;
using System.Collections.Generic;
using System.Text;

namespace KafeinTest.Model.Validation
{
    public class UserValidation: AbstractValidator<User>
    {
        public UserValidation()
        {


            RuleFor(User => User.Name)
                .NotNull()
                .WithMessage("İsim alanı boş geçilemez");/*.Equal(User=> db.User.Any(x => x.Name == User.Name) ? User.Name : "")*/
            RuleFor(User => User.SurName)
                .NotNull()
                .WithMessage("Soyisim alanı boş geçilemez");
            RuleFor(User => User.Email)
                .NotNull()
                .EmailAddress()
                .WithMessage("Email alanı boş geçilemez, hatalı mail adresi girişi");
            RuleFor(User => User.BirthdayDate)
                .NotNull()
                .WithMessage("Doğum tarihi alanı bos geçilemez");
            RuleFor(User => User.Password)
                .NotNull()
                .MinimumLength(6)
                .WithMessage("pasaport alanı bos geçilemez, şifreniz minimum 6 karakter olabilir");
            RuleFor(User => User.LoginName)
                .NotNull()
                .MinimumLength(6)
                .WithMessage("Kullanıcı adı alanı bos geçilemez");
        

        }
    }
}
