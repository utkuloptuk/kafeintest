﻿using System;
using System.Collections.Generic;
using System.Text;

namespace KafeinTest.Model.DataTransferObject
{
    public class DTOTokenModel
    {
        public string Token { get; set; }
    }
}
