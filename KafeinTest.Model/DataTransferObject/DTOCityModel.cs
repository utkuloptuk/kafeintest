﻿using KafeinDbProject;
using System;
using System.Collections.Generic;
using System.Text;

namespace KafeinTest.Model.DataTransferObject
{
    public class DTOCityModel
    {
        public string cityName { get; set; }
        public int countyId { get; set; }
        public string countyName { get; set; }
    }
    
}
