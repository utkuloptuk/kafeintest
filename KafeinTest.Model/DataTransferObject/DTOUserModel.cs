﻿using KafeinDbProject;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace KafeinTest.Model.DataTransferObject
{
    public class DTOUserModel
    {
        public string LoginName { get; set; }
        public string Password { get; set; }
    }

    public class DTOUser:User
        {
        [NotMapped]
        public string CityName { get; set; }
        [NotMapped]
        public string BDate { get; set; }
    }
}
