﻿using KafeinDbProject;
using System;
using System.Collections.Generic;
using System.Text;

namespace KafeinTest.Interfaces
{
    public interface ICityManager
    {
        List<City> GetAll();
        City Get(int cityId);
        void Add(City city);
        void Delete(int cityId);
        void Update(City city);
    }
}
