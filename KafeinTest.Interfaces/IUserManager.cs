﻿using KafeinDbProject;
using System;
using System.Collections.Generic;
using System.Text;

namespace KafeinTest.Interfaces
{
    public interface IUserManager
    {
        List<User> GetAll();
        User Get(int userId);
        void Add(User user);
        void Delete(int userId);
        void Update(User user);
    }
}
