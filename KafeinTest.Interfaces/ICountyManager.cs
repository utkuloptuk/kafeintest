﻿using KafeinDbProject;
using System;
using System.Collections.Generic;
using System.Text;

namespace KafeinTest.Interfaces
{
    public interface ICountyManager
    {
        List<County> GetAll();
        County Get(int countyId);
        void Add(County county);
        void Delete(int countyId);
        void Update(County county);
    }
}
