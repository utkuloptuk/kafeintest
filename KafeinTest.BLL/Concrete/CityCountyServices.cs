﻿using KafeinDbProject;
using KafeinTest.DAL.Abstract;
using KafeinTest.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace KafeinTest.BLL.Concrete
{
    public class CityCountyServices : ICityCountyManager
    {
        private ICityCountyDal _ICityCountyDal;
        public CityCountyServices(ICityCountyDal cityCountyDal)
        {
            _ICityCountyDal = cityCountyDal;
        }

        public void Add(CityCounty cityCounty)
        {
            _ICityCountyDal.Add(cityCounty);
        }

        public void Delete(int cityCountyId)
        {
            _ICityCountyDal.Delete(cityCountyId);
        }

        public CityCounty Get(int cityCountyId)
        {
            return _ICityCountyDal.Get(cityCountyId);
        }

        public List<CityCounty> GetAll()
        {
            return _ICityCountyDal.GetAll();
        }

        public void Update(CityCounty cityCounty)
        {
            _ICityCountyDal.Update(cityCounty);
        }
    }
}
