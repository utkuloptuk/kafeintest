﻿using KafeinDbProject;
using KafeinTest.DAL.Abstract;
using KafeinTest.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace KafeinTest.BLL.Concrete
{
    public class CountyServices : ICountyManager
    {
        private ICountyDal _ICountyDal;
        public CountyServices(ICountyDal countyDal)
        {
            _ICountyDal = countyDal;
        }

        public void Add(County county)
        {
            _ICountyDal.Add(county);
        }

        public void Delete(int countyId)
        {
            _ICountyDal.Delete(countyId);
        }

        public County Get(int countyId)
        {
            return _ICountyDal.Get(countyId);
        }

        public List<County> GetAll()
        {
            return _ICountyDal.GetAll();
        }

        public void Update(County county)
        {
            _ICountyDal.Update(county);
        }
    }
}
