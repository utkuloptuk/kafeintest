﻿using KafeinDbProject;
using KafeinTest.DAL.Abstract;
using KafeinTest.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace KafeinTest.BLL.Concrete
{
    public class CityServices:ICityManager
    {
        private ICityDal _ICityDal;
        public CityServices(ICityDal cityDal)
        {
            _ICityDal = cityDal;
        }

        public void Add(City city)
        {
            _ICityDal.Add(city);
        }

        public void Delete(int cityId)
        {
            _ICityDal.Delete(cityId);
        }

        public City Get(int cityId)
        {
            return _ICityDal.Get(cityId);
        }

        public List<City> GetAll()
        {
            return _ICityDal.GetAll();
        }

        public void Update(City city)
        {
            _ICityDal.Update(city);
        }
    }
}
