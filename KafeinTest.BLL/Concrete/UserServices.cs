﻿using KafeinDbProject;
using KafeinTest.DAL.Abstract;
using KafeinTest.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace KafeinTest.BLL.Concrete
{
    public class UserServices : IUserManager
    {
        private IUserDal _IUserDal;
        public UserServices(IUserDal userDal)
        {
            _IUserDal = userDal;
        }

        public void Add(User user)
        {
            _IUserDal.Add(user);
        }

        public void Delete(int userId)
        {
            _IUserDal.Delete(userId);
        }

        public User Get(int userId)
        {
            return _IUserDal.Get(userId);
        }

        public List<User> GetAll()
        {
            return _IUserDal.GetAll();
        }

        public void Update(User user)
        {
            _IUserDal.Update(user);
        }
    }
}
